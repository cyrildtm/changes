# changes

Änderungsmarkierung für LaTeX.

Change markup for LaTeX.

----

Das changes-Paket dient zur manuellen Markierung von geändertem Text, insbesondere Einfügungen, Löschungen und Ersetzungen.
Der geänderte Text wird farbig markiert und, bei gelöschtem Text, durchgestrichen.
Zusätzlich kann Text hervorgehoben und/oder kommentiert werden.
Das Paket ermöglicht die freie Definition von Autoren und deren zugeordneten Farben.
Es erlaubt zusätzlich die Änderung des Änderungs-, Autor-, Hervorhebungs- und Kommentarmarkups.

Das Paket ist in den üblichen LaTeX-Distributionen enthalten und kann darüber installiert werden.

The changes-package allows the user to manually markup changes of text, such as additions, deletions, or replacements.
Changed text is shown in a different color; deleted text is striked out.
Additionally, text can be highlighted and/or commented.
The package allows free definition of additional authors and their associated color.
It also allows you to change the markup of changes, authors, highlights or comments.

The package is contained in the common LaTeX distributions, thus it can be installed via these distributions.

[changelog](changelog.md)

## Internet

- <https://www.ctan.org/pkg/changes> CTAN-Beschreibung / CTAN description
- <http://edgesoft.de/projects/latex/changes/> Projektwebseite (German)
- <http://changes.sourceforge.net/> Project Website (English)
- <https://sourceforge.net/projects/changes/files/> Downloads via sourceforge

## Nutzung/Usage

- Nutzerhandbuch: <https://gitlab.com/ekleinod/changes/raw/master/texmf/doc/latex/changes/changes.ngerman.pdf>
- User Manual: <https://gitlab.com/ekleinod/changes/raw/master/texmf/doc/latex/changes/changes.english.pdf>

## Git-Repository

Das Branching-Modell orientiert sich am "stable mainline model", das in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/> beschrieben ist.

Das bedeutet, es gibt immer einen stabilen Hauptzweig, den master.Branch.
Dieser Branch ist immer ohne Fehler benutzbar.

Features werden in Feature-Branches entwickelt.
Für die Finalisierung von Releases werden, abweichend vom Mainline-Modell, spezielle Feature-Branches genutzt.

Releases werden als Branches des Hauptzweigs erzeugt, zusätzlich wird jedes Release getaggt.
Die Tags enthalten die Patch-Information und, falls nötig, zusätzliche Identifier wie `rc1` oder `beta`.

Patches werden im zugehörigen Release-Branch vorgenommen.
Minor-Versionen bekommen ihren eigenen Release-Branch.

----

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the master branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Rechtliches/Legal stuff

### Lizenzen/Licenses

- LaTeX Project Public License (LPPL), siehe Datei [LICENSE](LICENSE).
- LaTeX Project Public License (LPPL), see file [LICENSE](LICENSE).

### Copyright

	README.md
	Copyright 2007-2019 Ekkart Kleinod <ekleinod@edgesoft.de>

	This work may be distributed and/or modified under the
	conditions of the LaTeX Project Public License, either version 1.3
	of this license or (at your option) any later version.
	The latest version of this license is in
		http://www.latex-project.org/lppl.txt
	and version 1.3 or later is part of all distributions of LaTeX
	version 2005/12/01 or later.

	This work has the LPPL maintenance status 'maintained'.

	The Current Maintainer of this work is Ekkart Kleinod.

	This work consists of the files

	source/latex/changes/changes.drv
	source/latex/changes/changes.dtx
	source/latex/changes/changes.ins
	source/latex/changes/examples.dtx
	source/latex/changes/README
	source/latex/changes/userdoc/*.tex

	scripts/changes/pyMergeChanges.py

	and the derived files

	doc/latex/changes/changes.english.pdf
	doc/latex/changes/changes.english.withcode.pdf
	doc/latex/changes/changes.ngerman.pdf

	doc/latex/changes/examples/changes.example.*.tex
	doc/latex/changes/examples/changes.example.*.pdf

	tex/latex/changes/changes.sty
